<style>
  body {
    padding-top: 100px;
    padding-bottom: 40px;
    background-color: #eee;
  }

  .form-signin {
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox {
    margin-bottom: 10px;
  }
  .form-signin .checkbox {
    font-weight: normal;
  }
  .form-signin .form-control {
    position: relative;
    height: auto;
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
    padding: 10px;
    font-size: 16px;
  }
  .form-signin .form-control:focus {
    z-index: 2;
  }
  .form-signin input[type="email"] {
    margin-bottom: -1px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }
  .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
</style>

<script type="text/JavaScript" src="js/sha512.js"></script>
<script type="text/JavaScript" src="js/forms.js"></script>

<form class="form-signin" action="spacetask/loginproc.php" method="post" name="login_form">

<?php
  include_once 'dbcon.php';
  include_once 'loginfunctions.php';

  if (isset($_GET['error'])) {
    echo '<div class="alert alert-danger alert-dismissible" role="alert">';
    echo '  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    echo '  <strong>Oops!</strong> Invalid login.';
    echo '</div>';
  }
?>
  <h2 class="form-signin-heading">Please sign in</h2>
  <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="email" class="form-control" placeholder="Email address" required autofocus>
  <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
  <button class="btn btn-lg btn-primary btn-block" id="submitbutton" type="button" onclick="formhash(this.form, this.form.password);">Sign in</button>
</form>
<script>
$("#email").keyup(function(event){
    if(event.keyCode == 13){
        $("#submitbutton").click();
    }
});
$("#password").keyup(function(event){
    if(event.keyCode == 13){
        $("#submitbutton").click();
    }
});
</script>
