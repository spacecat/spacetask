<?php
//SpaceTask configuration file
$GLOBALS['spacetask-task-name-maxlen'] = 250;     //maximum length of a task name
$GLOBALS['spacetask-task-desc-maxlen'] = 2048;    //maximum length of a task description
$GLOBALS['spacetask-task-children-maxlen'] = 2048; //maximum length of task's children list - CHARACTERS, NOT CHILDREN COUNT

$GLOBALS['spacetask-group-name-maxlen'] = 50;     //maximum length of a group name
$GLOBALS['spacetasl-group-desc-maxlen'] = 1024;   //maximum lenght of a group description

$GLOBALS['spacetask-journal-name-maxlen'] = 250;
$GLOBALS['spacetask-journal-cont-maxlen'] = 4096; //maximum length of a journal entry;

$GLOBALS['spacetask-user-free-tasklimit'] = 200;  //maximum amount of tasks per free user
$GLOBALS['spacetask-user-free-grouplimit'] = 10;  //maximum amount of groups per free user
$GLOBALS['spacetask-user-free-journallimit'] = 50;  //maximum amount of journal entries per free user

$GLOBALS['spacetask-user-pro-tasklimit'] = 10000; //maximum amount of tasks per pro user
$GLOBALS['spacetask-user-pro-grouplimit'] = 1000; //maximum amount of groups per pro user
$GLOBALS['spacetask-user-pro-journallimit'] = 10000;  //maximum amount of journal entries per pro user

?>
