<?php
include_once "request_functions.php";
include_once "loginfunctions.php";

sec_session_start();  //start a secure session

//
// Brief anatomy of the possible requests:
//-----------------------------------------
// - All request are POST requests.
// - EVERY request must contain an 'op' parameter, which contains which
//    operation is supposed to be done with the other parameters.
// - The other parameters are to be determined at this point.
//
// - The response is always in JSON format. It's an array containing two fields.
//    One of the fields is 'stat' - this always contains a status response of the function.
//    The other field is usually 'data' - this usually contains an associative array
//     of all sorts of stuff, based on what you asked for in the first place.
//

// List of all known functions:
//---------------------------------------------------------------------------------------------------
// OP NAME              Arguments                             Description
// --------------------------------------------------------------------------------------------------
//*task.new             params[]                              Creates a new task wuth set parameters
// task.get             taskid[]                              Gets chosen tasks
// task.modify          taskid, params[]                      Modifies task's parameters
// task.modify.field    taskid, field, value                  Modifies the selected field of a task
// task.update          taskid[]                              Update task's completion based on its children
// task.update.all      none                                  Updates all user's tasks - this call should be time limited... probably
// task.list.all        none                                  Lists all user's tasks
// task.list.root       none                                  Lists all user's tasks that are roots of the task trees
// task.list.parents    taskid                                Lists all parent tasks to this task //not actually sure how helpful this is
// task.list.dlinein    deadline-datetime                     Lists all tasks with a deadline within chosen deadline
// task.complete        taskid[]                              Change completion state of a LEAF task - a task with no child tasks.
// task.delete          taskid[]                              Delete a task.
// group.new            groupname, colour                     Create a new, empty group.
// group.modify         groupid, groupname, colour            Modifies a group's name and colour.
// group.insert         groupid, taskid[]                     Inserts a task to chosen group.
// group.list.groups    none                                  Lists all user's groups
// group.list.tasks     groupid                               Lists all user's tasks within a chosen group.
// group.list.tasks.root  groupid                             Lists all user's root tasks in a chosen group.
// group.delete         groupid[]                             Deletes selected groups
// journal.new          title, content                        Creates a new journal entry
// journal.modify       journalid, title, content             Modifies a chosen journal entry
// journal.bindtask     journalid[], taskid[]                   Binds a journal entry to selected tasks
// journal.bindgroup    journalid[], groupid[]                  Binds a journal entry to selected groups\
// journal.get          journalid[]                           Gets chosen journal entries
// journal.list.all     none                                  Lists all user's journal entries
// journal.list.orphans none                                  Lists all user's unbound journal entries
// journal.list.task    taskid[]                              Lists all journal entries for chosen tasks
// journal.list.group   groupid[]                             Lists all journal entries for chosen groups
// journal.delete       journalid[]                           Deletes chosen journal entries



if(array_key_exists('q', $_POST)){
  //valid something, woo.
  //TODO: actual request processing

  $response = array(
    'stat' => 'ERROR_OOPS',
    'data' => array(
      'errmsg' => 'Something went wrong when processing last request.',
    )
  );


  $uid = RequestController::getUserID();

  error_log("UID=" . $uid);

  if($uid != null){
    switch($_POST['q']){
      //TODO: bother implementing all those functions
      case 'task-new':
        try{
          $newid = RequestController::newTask($uid, json_decode($_POST['params']));
          $response['data'] = $newid;
          $response['stat'] = 'OK';
        }catch(Exception $e){
          $response['data']['errrmsg'] = $e->getMessage();
        }
        break;
      case 'taskget':
        try{
          $response['data'] = RequestController::getTask($uid, /*json_decode*/($_POST['taskid']));
          $response['stat'] = 'OK';
        }catch(Exception $e){
          $response['data']['errrmsg'] = $e->getMessage();
        }
        break;
      case 'hi':
        $response = array('stat' => 'OK', 'data'=>"Hello!");
        break;
      default:
        $response = array('stat' => 'ERROR_WTF', 'data' => array('errmsg' => 'Unsupported function.'));
        break;
    }
  }else{
    $response = array(
      'stat' => 'ERROR_LOGGEDOUT',
      'data' => array(
        'errmsg' => 'Logged out',
      )
    );
  }
  echo json_encode($response);
}else{
  //user is not logged in or there was absolutely nothing in post, or it was invalid.
  echo json_encode(array(
    'stat' => 'ERROR_FUQ?',
    'data' => array(
      'errmsg' => 'Invalid request.',
    )
  ));
}
?>
