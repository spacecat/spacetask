<?php
// request_functions.php
// ----------------
// this file should contain all functions related to handling the tasks,
//  groups and journals in the database, as well as some utility functions.
// these functions are to be called by the AJAX requests.

include "spacetaskconfig.php";
include_once "dbcon.php";

//TODO:
// - remake note storage
// - add note getting
// - add getting tasks with a specified deadline
// - add getting user's groups
// - add creating user's groups
// - add everything else that's missing from the requests.php
//

// List of all functions within this file:
//------------------------------------------
//TODO: fill this list up

class RequestController {

  //task.new
  public function newTask($userid, $params){
    //params - array with following indexes:
    // 'name'     - task name
    // 'desc'     - task description
    // 'deadline' - datetime of project deadline. set to arbitrarily high number if it has no deadline

    $mysqli = $GLOBALS['glob-mysqli'];

    //check if all required parameters are actually present in the $params
    $reqdparams = array('name', 'desc', 'deadline', 'children', 'weight');
    foreach ($reqdparams as $rqpar) {
      if(!array_key_exists($rqpar, $params)){
        return "Wrong parameters supplied!";
      }
    }
    //end of parameter check

    //check if name, desc and children have a supported length
    if(strlen($params['name']) > $GLOBALS['spacetask-task-name-maxlen']){
      return "Task name too long!";
    }
    if(strlen($params['desc']) > $GLOBALS['spacetask-task-desc-maxlen']){
      return "Description too long!";
    }
    if(strlen($params['children']) > $GLOBALS['spacetask-task-children-maxlen']){
      return "Too long list of children!";
    }
    //end of length check

    //check if the user is actually able to add more tasks
    if(self::getTaskCount($userid) >= self::getTaskLimit($userid)){
      return "Can't add more tasks!";
    }

    try{
      $stmt = $mysqli->prepare("INSERT INTO `tasks`(`userid`, `taskname`, `taskdesc`, `deadline`, `children`, `weight`)
              VALUES(?, ?, ?, ?, ?, ?)");
      $stmt->bind_param("issssi", $userid, $params['name'], $params['desc'], $params['deadline'], $params['children'], $params['weight']);
      $stmt->execute();
      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER
    }catch(Exception $e){
      error_log($e);
      return null;
    }

    return $mysqli->insert_id;
  }

  //task.modify
  public function modifyTask($userid, $taskid, $params){
    $mysqli = $GLOBALS['glob-mysqli'];

    //check if all required parameters are actually present in the $params
    $reqdparams = array('name', 'desc', 'deadline', 'children', 'weight');
    foreach ($reqdparams as $rqpar) {
      if(!array_key_exists($rqpar, $params)){
        return "Wrong parameters supplied!";
      }
    }
    //end of parameter check

    //check task ownership
    if(!self::checkTaskOwnership($userid, $taskid)){
      return "Tried to modify task you don't own!";
    }

    //check if name, desc and children have a supported length
    if(strlen($params['name']) > $GLOBALS['spacetask-task-name-maxlen']){
      return "Task name too long!";
    }
    if(strlen($params['desc']) > $GLOBALS['spacetask-task-desc-maxlen']){
      return "Description too long!";
    }
    if(strlen($params['children']) > $GLOBALS['spacetask-task-children-maxlen']){
      return "Too long list of children!";
    }
    //end of length check

    try{
      $stmt = $mysqli->prepare("UPDATE tasks SET taskname = ?,taskdesc = ?,deadline = ?,children = ?,weight = ? WHERE taskid = ? AND userid = ?");
      $stmt->bind_param("ssssiii", $params['name'], $params['desc'], $params['deadline'], $params['children'], $params['weight'], $taskid, $userid);
      $stmt->execute();
      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER
    }catch(Exception $e){
      error_log($e);
      return null;
    }

    return true;
  }

  //task.get
  public function getTask($userid, $taskid_arr){
    $mysqli = $GLOBALS['glob-mysqli'];

    $taskid_csv = self::convertArrUniquesToCSV($taskid_arr);
    var_dump($taskid_csv);

    try{
      $stmt = $mysqli->prepare("SELECT * FROM tasks WHERE taskid IN($taskid_csv) AND userid = ?");
      $stmt->bind_param('i', $userid);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = array();
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret[] = $r;
        }
      }while($r);

      error_log(print_r($ret,1));

      return $ret;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //task.modify.field
  public function modifyTaskField($userid, $taskid, $property, $value){
    $mysqli = $GLOBALS['glob-mysqli'];

    if(!self::checkTaskOwnership($userid, $taskid)){
      return "Tried to modify task you don't own!";
    }

    //list of allowed properties and their types
    $allowed_prop = array(
      "name" => array('s', "taskname"),
      "desc" => array('s', "taskdesc"),
      "deadline" => array('s', "deadline"),
      "children" => array('s', "children"),
      "weight" => array('i', "weight"),
      "completion" => array('d', "completion"),
    );

    if($property == "name" && strlen($value) > $GLOBALS['spacetask-task-name-maxlen']){
      return "Task name too long!";
    }
    if($property == "desc" && strlen($value) > $GLOBALS['spacetask-task-desc-maxlen']){
      return "Description too long!";
    }
    if($property == "children" && strlen($value) > $GLOBALS['spacetask-task-children-maxlen']){
      return "Too long list of children!";
    }

    $actualprop = $allowed_prop[$property][1];

    if(array_key_exists($property, $allowed_prop)){
      try{
        $stmt = $mysqli->prepare("UPDATE tasks SET $actualprop = ? WHERE taskid = ? AND userid = ?");
        $stmt->bind_param($allowed_prop[$property][0] . 'ii', $value, $taskid, $userid);
        $stmt -> execute();  //exe-kyoot :3
      }catch(Exception $e){
        error_log($e);
        return null;
      }
    }else{
      return "Unknown property.";
    }

    return true;
  }

  //TODO: fix this thing so it actually makes sense
  //task.update
  public function updateTask($userid, $taskid){
    //taskid    - task ID

    //TODO: fix this thing, it's a mess.
    $mysqli = $GLOBALS['glob-mysqli'];

    //what it does:
    //  it reads all the task's children's progresses (just the first level),
    //   sums it by their weights and then saves the result to the dbase.
    //   the result must always be between 0 and 1

    $comp_sum = 0;    //total of all completions of subtasks
    $weight_sum = 0;  //total of all weights of subtasks

    $children = rq_task_list_children($userid, $taskid);

    $completion = null;  //resulting completion status

    try{
      $stmt = $mysqli->prepare("SELECT weight, completion FROM tasks WHERE taskid IN($children) AND userid = ?");
      $stmt->bind_param('i', $userid);
      error_log($mysqli->error);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = array();
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret[] = $r;
          $comp_sum += $r['completion'] * $r['weight'];
          $weight_sum += $r['weight'];
          print_r($r);
        }
      }while($r);
      if(strlen($children) != 0){
        $completion = ($comp_sum)/$weight_sum;
      }else{
        $completion = -1;  //this means this node is probably a leaf - end node
      }
    }catch(Exception $e){
      error_log($e);
      return false; //an error has occured... how sad.
    }

    try{
      $stmt = $mysqli->prepare("UPDATE tasks SET completion = ? WHERE taskid = ?");
      $stmt->bind_param("di", $completion, $taskid);
      error_log($mysqli->error);
      $stmt->execute();
    }catch(Exception $e){
      error_log($e);
    }
  }

  //task.update.all
  public function updateAllTasks($userid){
    //TODO: actual code
  }

  //task.list.all
  public function listAllTasks($userid){
    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT * FROM tasks WHERE userid = ?");
      $stmt->bind_param("i", $userid);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = array();
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret[] = $r;
        }
      }while($r);
      return $ret;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //task.list.root
  public function listRootTasks($userid){
    //gets the tasks at the top of the tree - those that aren't pointed to as children anywhere

    $parented = self::listParentedTasks($userid); //we're selecting tasks that are not parented, so not those...

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT * FROM tasks WHERE userid = ? AND taskid NOT IN($parented)");
      $stmt->bind_param("i", $userid);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = array();
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret[] = $r;
        }
      }while($r);
      return $ret;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //task.list.parents
  public function listParentTasks($userid, $taskid){
    //returns   array of task IDs that are parents of this task

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT * FROM tasks WHERE children LIKE '%$taskid%' AND userid = ?");
      $stmt->bind_param($userid);
      error_log($mysqli->error);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = array();
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret[] = $r;
        }
      }while($r);
      return $ret;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //task.list.dlinein
  public function getTaskWithDeadlineIn($userid, $datetime){
    //TODO: actual code
  }

  //task.complete
  public function completeTask($userid, $taskid_arr){
    $mysqli = $GLOBALS['glob-mysqli'];

    $taskid_csv = self::convertArrUniquesToCSV($taskid_arr);

    try{
      $stmt = $mysqli->prepare("UPDATE tasks SET completion=1 WHERE taskid IN($taskid_csv)");
      $stmt->bind_param("i", $userid);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = array();
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret[] = $r;
        }
      }while($r);
      return $ret;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //task.delete
  public function deleteTasks($userid, $taskid_arr){
    //TODO: add the actual code
    $mysqli = $GLOBALS['glob-mysqli'];

    //check ownership
    foreach ($taskid_arr as $tid) {
      if(!self::checkTaskOwnership($userid, $tid)){
        return "Can't delete tasks you don't own!";
      }
    }

    $taskid_csv = self::convertArrUniquesToCSV($taskid_arr);

    try{
      $stmt = $mysqli->prepare("DELETE FROM tasks WHERE userid = ? AND taskid IN($taskid_csv)");
      $stmt->bind_param("i", $userid);
      $stmt->execute();

      return $mysqli->error_get_last;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //group.new
  public function newGroup($userid, $params){
    //TODO: add the actual code
    $mysqli = $GLOBALS['glob-mysqli'];

    //check if all required parameters are actually present in the $params
    $reqdparams = array('name', 'desc', 'colour');
    foreach ($reqdparams as $rqpar) {
      if(!array_key_exists($rqpar, $params)){
        return "Wrong parameters supplied!";
      }
    }

    //TODO: check for colour validity

    //end of parameter check

    //check if name and desc have a supported length
    if(strlen($params['name']) > $GLOBALS['spacetask-task-name-maxlen']){
      return "Task name too long!";
    }
    if(strlen($params['desc']) > $GLOBALS['spacetask-task-desc-maxlen']){
      return "Description too long!";
    }

    //end of length check

    //check if the user is actually able to add more groups
    if(self::getGroupCount($userid) >= self::getGroupLimit($userid)){
      return "Can't add more groups!";
    }

    try{
      $stmt = $mysqli->prepare("INSERT INTO `groups`(`userid`, `groupname`, `groupdesc`)
              VALUES(?, ?, ?)");
      $stmt->bind_param("iss", $userid, $params['name'], $params['desc']);
      $stmt->execute();
      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER
    }catch(Exception $e){
      error_log($e);
      return null;
    }

    return $mysqli->insert_id;
  }

  //group.modify
  public function modifyGroup($userid, $groupid, $params){
    $mysqli = $GLOBALS['glob-mysqli'];

    //check if all required parameters are actually present in the $params
    $reqdparams = array('name', 'desc', 'colour');
    foreach ($reqdparams as $rqpar) {
      if(!array_key_exists($rqpar, $params)){
        return "Wrong parameters supplied!";
      }
    }

    //TODO: check for colour validity

    //end of parameter check

    //check group ownership
    if(!self::checkGroupOwnership($userid, $taskid)){
      return "Tried to modify task you don't own!";
    }

    //check if name and desc have a supported length
    if(strlen($params['name']) > $GLOBALS['spacetask-group-name-maxlen']){
      return "Task name too long!";
    }
    if(strlen($params['desc']) > $GLOBALS['spacetask-group-desc-maxlen']){
      return "Description too long!";
    }
    //end of length check

    try{
      $stmt = $mysqli->prepare("UPDATE groups SET groupname = ?,groupdesc = ?,colour = ? WHERE groupid=? AND userid=$userid");
      $stmt->bind_param("sssii", $params['name'], $params['desc'], $params['colour'], $taskid, $userid);
      $stmt->execute();
      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER
    }catch(Exception $e){
      error_log($e);
      return null;
    }

    return true;
  }

  //group.list.tasks
  public function listGroupTasks($userid, $groupid){
    $mysqli = $GLOBALS['glob-mysqli'];

    $taskid_csv = self::convertArrUniquesToCSV($taskid_arr);
    var_dump($taskid_csv);

    try{
      $stmt = $mysqli->prepare("SELECT * FROM tasks WHERE taskid IN($taskid_csv) AND userid = ?");
      $stmt->bind_param('i', $userid);
      error_log($mysqli->error);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = array();
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret[] = $r;
        }
      }while($r);
      return $ret;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //group.insert
  public function addTasksToGroup($userid, $groupid, $taskid_arr){
    //TODO: add the actual code

  }

  //group.delete
  public function deleteGroups($userid, $groupid_arr){
    //TODO: add the actual code
  }

  //journal.new
  public function newJournal($userid, $params){
    //TODO: add the actual code
  }

  //journal.modify
  public function modifyJournal($userid, $journalid, $params){
    //TODO: add the actual code
  }

  //journal.bindtask
  public function bindJournalsTasks($userid, $journalid_arr, $taskid_arr){
    //TODO: add the actual code
  }

  //journal.bindgroup
  public function bindJournalsGroups($userid, $journalid_arr, $taskid_arr){
    //TODO: add the actual code
  }

  //journal.get
  public function getJournalEntries($userid, $journalid_arr){
    //TODO: add the actual code
  }

  //journal.list.all
  public function listAllJournals($userid){
    //TODO: add the actual code
  }

  //journal.list.orphans
  public function listOrphanedJournals($userid){  //gets journal entries that are unbound
    //TODO: add the actual code
  }

  //journal.list.task
  public function listJournalTasks($userid, $taskid_arr){
    //TODO: add the actual code
  }

  //journal.list.group
  public function listJournalGroups($userid, $groupid_arr){
    //TODO: add the actual code
  }

  //journal.delete
  public function deleteJournal($userid, $journalid_arr){
    //TODO: add the actual code
  }

  //task.list.children - not an actual call yet!
  public function listChildTasks($userid, $taskid){
    //TODO: check task ownership

    //returns   array of task IDs that are children of this task

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT children FROM tasks WHERE taskid = ?");
      error_log($mysqli->error);
      $stmt->bind_param("i", $taskid);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = "";

      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret = rtrim($r['children'], ',');
        }
      }while($r);
      return $ret;
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //task.list.parented - not an actual call yet!
  public function listParentedTasks($userid){    //returns a comma-separated list of all user's taskID's that do have a parent
    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT children FROM tasks WHERE userid = ?");
      $stmt->bind_param("i", $userid);
      error_log($mysqli->error);
      $stmt->execute();
      $res = $stmt->get_result();

      $ret = "";
      do{
        $r = $res->fetch_assoc();
        if($r){
          $ret .= $r['children'];
        }
      }while($r);
      return rtrim($ret, ',');
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  //utility functions
  public function getUserID(){  //returns user ID if valid, if not, returns null
    if(login_check($GLOBALS['glob-mysqli'])){
      return $_SESSION['user_id'];
    }else{
      return null;
    }
  }

  public function checkTaskOwnership($userid, $taskid){   //returns true if taskid is actually owned by userid

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT userid FROM tasks WHERE taskid = ?");
      $stmt->bind_param("i", $taskid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      if($r['userid'] == $userid){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function checkGroupOwnership($userid, $groupid){
    //returns true if groupid is actually owned by userid

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT userid FROM groups WHERE groupid = ?");
      $stmt->bind_param("i", $groupid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      if($r['userid'] == $userid){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function checkJournalOwnership($userid, $journalid){
    //returns true if journalid is actually owned by userid

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT userid FROM groups WHERE journalid = ?");
      $stmt->bind_param("i", $journalid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      if($r['userid'] == $userid){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function convertArrUniquesToCSV($arr){
    //remove double occurences first
    //TODO: actually use that in all teh functions...

    $uniques = array();
    foreach ($arr as $v) {
      $uniques[$v] = null;
    }
    unset($arr);  //save a tiny bit of memory maybe.

    //convert array to CSV string
    $ret = "";
    foreach ($uniques as $key => $unused) {
      $ret .= $key . ',';
    }
    return rtrim($ret, ',');  //remove the last comma
  }

  //user group handling basic utils
  public function getTaskLimit($userid){

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT class FROM members WHERE id = ?");
      $stmt->bind_param("i", $userid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      if($r['class'] == 'free'){
        return $GLOBALS['spacetask-user-free-tasklimit'];
      }elseif($r['class'] == 'pro') {
        return $GLOBALS['spacetask-user-pro-tasklimit'];
      }else{
        return null;
      }
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function getGroupLimit($userid){

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT class FROM members WHERE id = ?");
      $stmt->bind_param("i", $userid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      if($r['class'] == 'free'){
        return $GLOBALS['spacetask-user-free-grouplimit'];
      }elseif($r['class'] == 'pro') {
        return $GLOBALS['spacetask-user-pro-grouplimit'];
      }else{
        return null;
      }
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function getJournalLimit($userid){

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT class FROM members WHERE id = ?");
      $stmt->bind_param("i", $userid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      if($r['class'] == 'free'){
        return $GLOBALS['spacetask-user-free-journallimit'];
        }elseif($r['class'] == 'pro') {
        return $GLOBALS['spacetask-user-pro-journallimit'];
      }else{
        return null;
      }
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function getTaskCount($userid){

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT COUNT(*) AS taskcount FROM tasks WHERE userid = ?");
      $stmt->bind_param("i", $userid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      return $r['taskcount'];
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function getGroupCount($userid){

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT COUNT(*) AS grpcount FROM groups WHERE userid = ?");
      $stmt->bind_param("i", $userid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      return $r['grpcount'];
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }

  public function getJournalCount($userid){

    $mysqli = $GLOBALS['glob-mysqli'];

    try{
      $stmt = $mysqli->prepare("SELECT COUNT(*) AS jcount FROM journals WHERE userid = ?");
      $stmt->bind_param("i", $userid);

      error_log($mysqli->error);  //DEV ONLY - REMOVE LATER

      $stmt->execute();
      $res = $stmt->get_result();
      $r = $res->fetch_assoc();
      return $r['jcount'];
    }catch(Exception $e){
      error_log($e);
      return null;
    }
  }
}

?>
