<?php
// navbar.php
// ----------------
// this file should only contain the navbar and some stuff related to that.
// it also contains the login inits and
//
?>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" id="nav-home">SpaceTask</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <?php
        include_once 'dbcon.php';
        include_once 'loginfunctions.php';

        //start the secure session
        sec_session_start();
        //check if the user is logged in
        if (login_check($GLOBALS['glob-mysqli']) === true) {
          //surprisingly they are, show their user button and a log out as well.
          echo '<ul class="nav navbar-nav">
            <li><a href="#" id="nav-link-overview" class="active"
                  data-toggle="tooltip" data-placement="bottom" title="Overview">
                <span class="glyphicon glyphicon-eye-open">
                </span> </a></li>
            <li><a href="#" id="nav-link-tasks"
                  data-toggle="tooltip" data-placement="bottom" title="Tasks">
            <span class="glyphicon glyphicon-tasks"></span> </a></li>
            <li><a href="#" id="nav-link-groups"
                  data-toggle="tooltip" data-placement="bottom" title="Groups">
            <span class="glyphicon glyphicon-list"></span> </a></li>
            <li><a href="#" id="nav-link-journal"
                  data-toggle="tooltip" data-placement="bottom" title="Journal">
            <span class="glyphicon glyphicon-book"></span> </a></li>
            <li><a href="#" id="nav-link-filter"
                  data-toggle="tooltip" data-placement="bottom" title="Filter">
            <span class="glyphicon glyphicon-filter"></span> </a></li>
            <li><a href="#" id="nav-link-new"
                  data-toggle="tooltip" data-placement="bottom" title="New&nbsp;task">
            <span class="glyphicon glyphicon-plus"></span> </a></li>
          </ul>';
          echo '<ul class="nav navbar-nav navbar-right">
                  <li><a href="#" id="nav-link-user"><span class="glyphicon glyphicon-user"></span> ' . htmlentities($_SESSION["username"]) . '</a></li>
                  <li><a href="spacetask/logout.php"><span class="glyphicon glyphicon-log-out"></span></a></li>
                  </ul>';
        }else{
          //apparently they're not, offer a sign in page
          echo '<ul class="nav navbar-nav navbar-right">';
          echo '  <li><a href="#" id="nav-link-signin"><span class="glyphicon glyphicon-log-in"></span> Sign in</a></li>';
          echo '  <li><a href="#" id="nav-link-register"><span class="glyphicon glyphicon-user"></span> Register</a></li>';
          echo '</ul>';
        }
      ?>
    </div>
  </div>
</nav>
