<style>
  body {
    padding-top: 50px;
    padding-bottom: 40px;
    background-color: #eee;
  }

  .form-signin {
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox {
    margin-bottom: 10px;
  }
  .form-signin .checkbox {
    font-weight: normal;
  }
  .form-signin .form-control {
    position: relative;
    height: auto;
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
    padding: 10px;
    font-size: 16px;
  }
  .form-signin .form-control:focus {
    z-index: 2;
  }
</style>

<?php
include_once 'register.inc.php';
include_once 'loginfunctions.php';

if (!empty($error_msg)) {
  echo '<div class="alert alert-danger alert-dismissible" role="alert">';
  echo '  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
  echo $error_msg;
  echo '</div>';
}
?>

<form class="form-signin" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>"
        method="post"
        name="registration_form">

    <h2 class="form-signin-heading">Please register</h2>

    <label for="username" class="sr-only">Username</label>
    <input type="text" name="username" id="username" class="form-control" placeholder="Username" required autofocus>

    <label for="email" class="sr-only">Email address</label>
    <input type="email" name="email" id="email" class="form-control" placeholder="Email address" required>

    <label for="password" class="sr-only">Password</label>
      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>

    <label for="confirmpwd" class="sr-only">Confirm password</label>
      <input type="password" id="confirmpwd" name="confirmpwd" class="form-control" placeholder="Confirm password" required>

      <input type="button" class="btn btn-lg btn-primary btn-block" type="button" value="Register"
           onclick="return regformhash(this.form,
                           this.form.username,
                           this.form.email,
                           this.form.password,
                           this.form.confirmpwd);" />
</form>
