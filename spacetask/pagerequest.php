<?php
// pagerequest.php
// ----------------
// this file should contain everything related to getting back
//  page contents from the server, so one can load pages over js and maybe save some server load and bandwidth... who knows.
//

if(isset($_GET["p"])){
  $req = $_GET["p"];

  //array of allowed files - this is just to prevent getting whatever from the server
  $files = array(
    "signin" => "login.php",
    "register" => "register.php"
  );

  if(isset($files[$req])){
    include $files[$req];
  }else{
    echo "404 " . $req; //return a file not found message
  }
}else{
  echo "404"; //at least show something
}

?>
