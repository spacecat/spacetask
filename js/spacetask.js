// spacetask.js
// ----------------
// requires: jQuery
// ----------------
// this file should contain a whole bunch of functions related
//  to the general functionality of spacetask
//

function initNavTabFunctions () {
  var tablinks = document.querySelectorAll('[id^="nav-link-"]');
  var i;
  for(i=0; i<tablinks.length; i++){
    if(i == 0){
      switchTabs(tablinks[i]);
    }
    tablinks[i].onclick = function (){switchTabs(this);};
  }
}


function switchTabs ( ref ) {

  document.getElementById("content").innerHTML += requestStuff({'q':'taskget', 'taskid':[1, 2, 3, 5]});
  document.getElementById("content").innerHTML += requestStuff({'q':'hi'});

  //TAB RESTYLING - making the unselected ones inactive
  var i;
  var tablinks = document.querySelectorAll('[id^="nav-link-"]');
  for(i=0; i<tablinks.length; i++){
    tablinks[i].parentElement.className = "";
  }

  //TAB RESTYLING - making the selected one active
  ref.parentElement.className = "active";

  var name = ref.id.substr("nav-link-".length);

  //load the page
  $("#loader").css("display", "block");
  $.get('spacetask/pagerequest.php', {p: name}, function(data){
    $("#content").html(data);
    $("#loader").css("display", "none");
  });

  try{
    var tabcontents = document.querySelectorAll('[id^="nav-cont-"]');
    for(i=0; i<tablinks.length; i++){
      $("#tab-cont-" + ref.id.substr("nav-link-".length)).style = "display: none";
    }
    var contelement = "#tab-cont-" + name;


    $(contelement).style = "display: block";
  }catch(e){}
}

initNavTabFunctions();

//TODO: DEV ONLY - THIS IS TO BE REMOVED
//TODO: replace with actual function calls and stuff
//TODO: start actually working on the actual pages for fuck's sake...

function requestStuff(aParams){
  var returnvalue = null;
  $.post('spacetask/requests.php', aParams, function(data) {
    returnvalue = data;
    console.log(data);
    console.log(data['data']);
    var items = [];
    $.each( data, function( key, val ) {
        items.push( "<li id='" + key + "'>" + key + " = " + val + "</li>" );
    });

    $( "<ul/>", {
      "class": "my-new-list",
      html: items.join( "" )
    }).appendTo( "body" );
  }, 'json');
  return returnvalue;
}


document.getElementById("content").innerHTML += "<script>alert('hello');</script>";
